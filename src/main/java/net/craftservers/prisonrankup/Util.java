package net.craftservers.prisonrankup;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Util {
	
	/*                                                                                   
	 *                                                                                   *
	 *  This source is under the right of All Rights Reserved,                           *
	 *  You may not copy or edit this code or distribute it in any way without a license * 
	 *  from Mazen K.                                                                    *
	 * Copyright 2014 CraftServers. All Rights Reserved.                               *
	 *                                                                                   *
	 */                                                           
	
	String rank = "";
	double cost = 0;
	
	
	Main main = Main.getPlugin();
	public void rankup(Player player){
		String prefix = ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Prefix") + " ");
		if(!TimeHandler.cooldown.contains(player)){
			if(canRankup(player)){
				whatRank(player);
				if(hasBalance(player)){
					if(!Main.getPlugin().getConfiguration().getString("users." + player.getName() + ".group").equalsIgnoreCase(Main.getPlugin().getConfiguration().getString("Last Rank"))){
						rankplayerUp(rank, cost, player);
						cost = 0;
						rank = "";
					}else{
						player.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Highest Rank MSG")));
					}
				}else{
						player.sendMessage(prefix + ChatColor.AQUA + "You need " + ChatColor.GOLD + "$" + Main.getPlugin().getConfiguration().getString("groups." + rank) + ChatColor.AQUA + " to rank up to " + ChatColor.GOLD + rank);
						cost = 0;
						rank = "";
				}
			}else{
				player.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "You cannot rankup, please contact an administrator about this.");
				System.out.println("[PrisonRankup] Player " + player.getName() + " was not able to rankup. Please check user.yml and if everything is intact with thats player profile please contact the plugin developer");
			}
		}else{
			String type = Main.getPlugin().getConfiguration().getString("Time type");
			if(type.equalsIgnoreCase("Seconds")){
				player.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "The required amount of time to rankup is: " + Main.getPlugin().getConfiguration().getDouble("Time Interval")  + " seconds!");
			}else if(type.equalsIgnoreCase("Hours")){
				player.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "The required amount of time to rankup is: " + Main.getPlugin().getConfiguration().getDouble("Time Interval") / 360 + " hours!");
			}else if(type.equalsIgnoreCase("Minutes")){
				player.sendMessage(prefix + ChatColor.RED + "" + ChatColor.BOLD + "The required amount of time to rankup is: " + Main.getPlugin().getConfiguration().getDouble("Time Interval") / 60 + " minutes!");
			}
		}
		Main.getPlugin().reloadConfiguration();
	}
	
	public void rankplayerUp(String group, double price, final Player player) {
		String prefix = ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Prefix") + " ");
		Main.economy.withdrawPlayer(player.getName(), price);
		Main.permission.playerRemoveGroup(player, Main.getPlugin().getConfiguration().getString("users." + player.getName() + ".group"));
		Main.permission.playerAddGroup(player, group);
		for(World w : Bukkit.getWorlds()) {
			Main.permission.playerRemoveGroup(w, player.getName(), Main.getPlugin().getConfiguration().getString("users." + player.getName() + ".group"));
			Main.permission.playerAddGroup(w, player.getName(), group);
		}
		Main.getPlugin().getConfiguration().set("users." + player.getName() + ".group", group);
		Main.getPlugin().saveConfiguration();
		Main.getPlugin().reloadConfiguration();
		Bukkit.broadcastMessage(prefix + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Rankup BC Message")).replaceAll("%player%", player.getName()).replaceAll("%rank%", rank));
		if(Main.getPlugin().getConfiguration().getBoolean("Timed Requirement") && Main.getPlugin().getConfiguration().getBoolean("Interval on all ranks")){
			TimeHandler.cooldown.add(player);
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("PrisonRankup"), new Runnable(){

				@Override
				public void run() {
					TimeHandler.cooldown.remove(player);
				}
				
			}, Main.getPlugin().getConfiguration().getInt("Time Interval") * 20);
		}
		cost = 0;
		rank = "";
	}

	private boolean hasBalance(Player player) {
		double balance = Main.economy.getBalance(player.getName());
		cost = Main.getPlugin().getConfiguration().getDouble("groups." + rank);
			if(balance >= cost){
				return true;
			}else{
				return false;
			}

	}
	
	public void getAvailableRanks(Player p) {
		Set<String> allRanks = Main.getPlugin().getConfiguration().getConfigurationSection("groups").getKeys(false);
		List<String> ranksAvailable = new ArrayList<String>();
		boolean isRankYet = false;
		for(String s: allRanks) {
			if(s.equalsIgnoreCase(Main.getPlugin().getConfiguration().getString("users." + p.getName() + ".group"))) {
				isRankYet = true;
			}else if(isRankYet) {
					ranksAvailable.add(formatRank(s));
			}else{
				
			}
		}
		int i = 0;
		for(String s: ranksAvailable) {
			++i;
			p.sendMessage(ChatColor.LIGHT_PURPLE + "" + i + ". " + s);
		}
	}
	
	private String formatRank(String key){
		String finPrice = Main.getPlugin().getConfiguration().getString("groups." + key);
		return ChatColor.AQUA + "[" + ChatColor.GOLD + key + ChatColor.AQUA + "]:" + " " + ChatColor.GREEN + "$" + finPrice;
	}

	private void whatRank(Player player) {
		rank = Main.getPlugin().getConfiguration().getString("users." + player.getName() + ".group");
		int count = 1;
		int max = 100;
		for(String key : Main.getPlugin().getConfiguration().getConfigurationSection("groups").getKeys(false)){
			if(count < max){
				if(key.equalsIgnoreCase(Main.getPlugin().getConfiguration().getString("users." + player.getName() + ".group"))){
					count = 110;
				}else{
					count++;
				}
			}else{
				rank = key;
				count = 0;
			}
		}
	}

	private boolean canRankup(Player player){
		if(!Main.getPlugin().getConfiguration().contains("users."+ player.getName())){
			createConfigProfile(player);
			return true;
		}else if(Main.getPlugin().getConfiguration().contains("users." + player.getName() + ".group")){
				return true;
		}else{
			createConfigProfile(player);
			return true;
		}
	}

	private void createConfigProfile(Player player){
		System.out.println("Unable to find " + player.getName() + "'s config profile!");
		System.out.println("Creating a new profile for " + player.getName() + "..");
		Main.getPlugin().getConfiguration().createSection("users" + "." + player.getName() + ".group");
		Main.getPlugin().saveConfiguration();
		Main.getPlugin().getConfiguration().set("users" + "." + player.getName() + ".group", Main.getPlugin().getConfiguration().getString("First Rank"));
		Main.getPlugin().saveConfiguration();
		System.out.println("Created new profile for " + player.getName() + " and reloaded configuration files");
	}
	
	public void newConfigRankProfile(Player player, String group, CommandSender sender){
		String prefix = ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Prefix") + " ");
		System.out.println("Creating a new profile for " + player.getName() + "..");
			if(!Main.getPlugin().getConfiguration().getConfigurationSection("users").contains(player.getName())){
				Main.getPlugin().getConfiguration().createSection("users" + "." + player.getName() + ".group");
				Main.getPlugin().saveConfiguration();
				if(Main.getPlugin().getConfiguration().getConfigurationSection("groups").contains(group)){
					Main.getPlugin().getConfiguration().set("users" + "." + player.getName() + ".group", Main.getPlugin().getConfiguration().getString("First Rank"));
					Main.getPlugin().saveConfiguration();
				}else{
					System.out.println("ERROR: Group given by " + sender.getName() + " is not defined in the Configuration File, easy fixes to this error has been sent to both " + player.getName() + " and " + sender.getName() + "..");
					sender.sendMessage(prefix + ChatColor.RED + "ERROR: Unable to set player " + player.getName() + "'s rank. Please note that these ranks are case-sensitive and that they must be defined in the configuration file. Please try reloading the config if the config has been done correctly.");
					player.sendMessage(prefix + ChatColor.RED + "ERROR: Unable to set your rank in the configuration files, please contact and admin about this.");
				}
			}else{
				sender.sendMessage(prefix + ChatColor.RED + " ERROR: Player config profile already exists, please use /rankup set [Player] [Rank defined in Config]");
			}
	}
	
	public void createAnonConfigRankProfile(String player, String group){
		System.out.println("Creating a new profile for " + player + "..");
			if(!Main.getPlugin().getConfiguration().getConfigurationSection("users").contains(player)){
				Main.getPlugin().getConfiguration().createSection("users" + "." + player + ".group");
				Main.getPlugin().saveConfiguration();
				if(Main.getPlugin().getConfiguration().getConfigurationSection("groups").contains(group)){
					Main.getPlugin().getConfiguration().set("users" + "." + player + ".group", group);
					Main.getPlugin().saveConfiguration();
				}else{
					System.out.println("ERROR: Group " + group + " is not defined in the Configuration File!");
				}
			}else{
				return;
			}
	}
	
	public void getPlayerProfile(CommandSender sender, Player player){
		sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Prefix")) + "---------");
		sender.sendMessage("");
		sender.sendMessage(ChatColor.AQUA + "Player's name: " + player.getDisplayName());
		if(player.isOnline()){
			sender.sendMessage(ChatColor.AQUA + "Player's online status: " + ChatColor.GOLD + "Online!");
		}else{
			sender.sendMessage(ChatColor.AQUA + "Player's online status: " + ChatColor.RED + "Offline!");
		}
		sender.sendMessage(ChatColor.AQUA + "Player's current group: " + ChatColor.GOLD + Main.getPlugin().getConfiguration().getString("users." + player.getName() + ".group"));
		sender.sendMessage(ChatColor.AQUA + "Player's current balance: " + ChatColor.GOLD + Main.economy.getBalance(player.getName()));
		sender.sendMessage("");
		sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Prefix")) + "---------");
	}
	
	public void setRank(Player player, CommandSender sender, String rank){
		String prefix = ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfiguration().getString("Prefix") + " ");
		System.out.println("Attempting to update " + player.getName() + "'s profile..");
		if(!Main.getPlugin().getConfiguration().contains("users." + player.getName() + ".group")){
			System.out.println("Unable to find " + player.getName() + "'s profile..");
			newConfigRankProfile(player, rank, sender);
		}else{
			if(!Main.getPlugin().getConfiguration().getConfigurationSection("groups").contains(rank)){
				System.out.println("ERROR: Group given by " + sender.getName() + " is not defined in the Configuration File, easy fixes to this error has been sent to both " + player.getName() + " and " + sender.getName() + "..");
				sender.sendMessage(prefix + ChatColor.RED + "ERROR: Unable to set player " + player.getName() + "'s rank. Please note that these ranks are case-sensitive and that they must be defined in the configuration file. Please try reloading the config if the config has been done correctly.");
				player.sendMessage(prefix + ChatColor.RED + "ERROR: Unable to set your rank in the configuration files, please contact and admin about this.");
			}else{
				Main.permission.playerRemoveGroup(player, Main.getPlugin().getConfiguration().getString("users" + "." + player.getName() + ".group"));
				Main.getPlugin().getConfiguration().set("users" + "." + player.getName() + ".group", rank);
				Main.getPlugin().saveConfiguration();
				Main.permission.playerAddGroup(player, rank);
				player.sendMessage(prefix + ChatColor.AQUA + "Successfully added rank " + ChatColor.GOLD + rank + ChatColor.AQUA + " to your profile!");
				sender.sendMessage(prefix + ChatColor.AQUA + "Successfully added rank " + ChatColor.GOLD + rank + ChatColor.AQUA + " to " + ChatColor.GOLD + player.getName() + ChatColor.AQUA + "'s profile!");
				System.out.println("Successfully updated " + player.getName() + "'s profile!");
			}
		}
		
	}
	
	
}
