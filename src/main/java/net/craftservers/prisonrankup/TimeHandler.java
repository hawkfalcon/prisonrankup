package net.craftservers.prisonrankup;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class TimeHandler implements Listener{
	
	/*                                                                                   
	 *                                                                                   *
	 *  This source is under the right of All Rights Reserved,                           *
	 *  You may not copy or edit this code or distribute it in any way without a license * 
	 *  from Mazen K.                                                                    *
	 * Copyright 2014 CraftServers. All Rights Reserved.                               *
	 *                                                                                   *
	 */                             
	
	int tid = 0;
	public static ArrayList<Player> cooldown = new ArrayList<Player>();
	
	Main main;
	public TimeHandler(Main instance){
		main = instance;
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		if(main.getConfig().getBoolean("Timed Requirement")){
			final Player player = event.getPlayer();
			cooldown.add(event.getPlayer());
	            tid = main.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("PrisonRankup"), new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						cooldown.remove(player);
					}
					
				}, Main.getPlugin().getConfig().getInt("Time Interval") * 20);
		}
	}
	
}
