package net.craftservers.prisonrankup;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import net.craftservers.prisonrankup.Updater.ReleaseType;
import net.craftservers.prisonrankup.Updater.UpdateResult;
import net.craftservers.prisonrankup.Updater.UpdateType;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	/*                                                                                   
	 *                                                                                   *
	 *  This source is under the right of All Rights Reserved,                           *
	 *  You may not copy or edit this code or distribute it in any way without a license * 
	 *  from Mazen K.                                                                    *
	 * Copyright � 2014 CraftServers. All Rights Reserved.                               *
	 *                                                                                   *
	 */                                                                                  
	
	public static boolean update = false;
	public static String name = "";
	public static ReleaseType type = null;
	public static String version = "";
	public static String link = "";
	public static Main plugin;

    @Override
	public void onDisable() {
    	plugin = null;
		getLogger().info("[PrisonRankup] Plugin has been disabled!");
	}
    Util util = new Util();
    PRLang lang = new PRLang(this);
	public void onEnable() {
		plugin = this;
		getServer().getPluginManager().registerEvents(new TimeHandler(this), this);
		getServer().getPluginManager().registerEvents(new SignHandler(this), this);
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new JoinHandler(), this);
    	getLogger().info("Plugin has been enabled!");
			getLogger().info("Attempting to link Vault.");
			try{
				setupPermissions();
				setupChat();
				setupEconomy();
			}catch (Exception e){
				e.printStackTrace();
				getLogger().info("Was unable to link Vault.");
			}
			getLogger().info("Vault has been linked!");
            
			File file = new File(getDataFolder(), "config.yml");
			getLogger().info("Checking if config.yml exist's..");
			if(!file.exists()){
				this.saveDefaultConfig();
				getLogger().info("Created a new config.yml!");
			}else{
				this.saveDefaultConfig();
	            getLogger().info("Config has been found and loaded!");
			}
			for(Player p : Bukkit.getOnlinePlayers()){
				if(!getConfig().getConfigurationSection("users").contains(p.getName())){
					for(String key : permission.getPlayerGroups(p)){
						if(getConfig().getConfigurationSection("groups").contains(key)){
							util.createAnonConfigRankProfile(p.getName(), key);
						}
					}
				}
			}
			  Updater updater = new Updater(this, 72819, this.getFile(), Updater.UpdateType.NO_DOWNLOAD, false); 
			  update = updater.getResult() == Updater.UpdateResult.UPDATE_AVAILABLE; 
			  name = updater.getLatestName(); 
			  version = updater.getLatestGameVersion(); 
			  type = updater.getLatestType(); 
			  link = updater.getLatestFileLink(); 
			}
	
    public static Permission permission = null;
    public static Economy economy = null;
    public static Chat chat = null;

    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    private boolean setupChat()
    {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            chat = chatProvider.getProvider();
        }

        return (chat != null);
    }

    private boolean setupEconomy()
    {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }
    String prefix = "" + ChatColor.translateAlternateColorCodes('&', "");
    public void reloadConfiguration(Player player){
    	getLogger().info("Attempting to reload config.");
    	player.sendMessage("" + prefix + ChatColor.GOLD + "Attempting to reload config");
    	File config = new File(getDataFolder(), "config.yml");
    	if(config.exists()){
    		this.reloadConfig();
    		player.sendMessage("" + prefix + ChatColor.AQUA + "Reloaded config.yml..");
    		getLogger().info("[PrisonRankup] Reloaded config.yml..");
    		player.sendMessage("" + prefix + ChatColor.GOLD + "Successfully reloaded all Configuration Files");
    	}else{
    		this.saveDefaultConfig();
    		player.sendMessage("" + prefix + ChatColor.RED + "ERROR: Unable to find config.yml");
    		player.sendMessage("" + prefix + ChatColor.AQUA + "Creating a new config.yml..");
			player.sendMessage("" + prefix + ChatColor.GOLD + "Successfully reloaded all Configuration Files");
    	}
    }
    
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
			if(command.getName().equalsIgnoreCase("rankup")){
				if(sender instanceof Player){
					Player player = (Player) sender;
					if(player.hasPermission("prisonrankup.rankup") || player.isOp()){
						if(args.length > 0){
							if(args[0].equalsIgnoreCase("reload")){
								if(player.hasPermission("prisonrankup.reload")){
									reloadConfiguration(player);
								}else{
									player.sendMessage(lang.noPermission);
								}
							}else if(args[0].equalsIgnoreCase("set")){
								if(args.length == 3){
									if(player.hasPermission("prisonrankup.set")){
										Player target = Bukkit.getPlayer(args[1]);
										if(target == null){
											player.sendMessage(prefix + ChatColor.RED + "ERROR: " + args[1] + " is null.");
										}else if(target.isOnline()){
											player.sendMessage(prefix + ChatColor.AQUA + "Attempting to set player " + args[1] + "'s new rank");
											util.setRank(target, player, args[2]);
										}
									}else{
										player.sendMessage(lang.noPermission);
									}
								}else{
									player.sendMessage(lang.error("ERROR: Incorrect syntax, please use /rankup set [Player] [Rank]"));
								}
							}else if(args[0].equalsIgnoreCase("create")){
								if(player.hasPermission("prisonrankup.create")){
									if(args.length == 3){
										Player target = Bukkit.getPlayer(args[1]);
										if(target == null){
											player.sendMessage(lang.error("ERROR: " + args[1] + " is null."));
										}else if(target.isOnline()){
											player.sendMessage(prefix + ChatColor.AQUA + "Creating new player profile..");
											util.newConfigRankProfile(target, args[2], sender);
											player.sendMessage(prefix + ChatColor.AQUA + "Created the new player profile!");
										}
									}else{
										player.sendMessage(lang.error("ERROR: Incorrect syntax, please use /rankup create [Player] [New Rank]"));
									}
								}else{
									player.sendMessage(lang.noPermission);
								}
							}else if(args[0].equalsIgnoreCase("get")){
								if(args.length == 2){
									Player target = Bukkit.getPlayer(args[1]);
									if(target == null){
										player.sendMessage(lang.error("ERROR: " + args[1] + " is null."));
									}else if(target.isOnline()){
										player.sendMessage(prefix + ChatColor.AQUA + "Getting players profile...");
										util.getPlayerProfile(sender, target);
									}
								}else{
									player.sendMessage(lang.error("ERROR: Incorrect syntax, please use /rankup get [Player] "));
								}
							}else if(args[0].equalsIgnoreCase("update")){
								if(player.hasPermission("prisonrankup.update")) {
									@SuppressWarnings("unused")
									Updater updater = new Updater(this, 72819, this.getFile(), Updater.UpdateType.NO_VERSION_CHECK, true);
									player.sendMessage(ChatColor.AQUA + "Updating plugin! Please check console for any errors/progress");
								}else{
									player.sendMessage(lang.noPermission);
								}
							}else{
								lang.description(player);
							}
						}else{
							util.rankup(player);
						}
					}else{
						player.sendMessage(lang.noPermission);
					}
				}else{
					if(args.length > 0){
						if(args[0].equalsIgnoreCase("set")){
							if(args.length == 3){
									Player target = Bukkit.getPlayer(args[1]);
									if(target == null){
										sender.sendMessage(prefix + ChatColor.RED + "ERROR: " + args[1] + " is null.");
									}else if(target.isOnline()){
										sender.sendMessage(prefix + ChatColor.AQUA + "Attempting to set player " + args[1] + "'s new rank");
										util.setRank(target, sender, args[2]);
									}
							}else{
								sender.sendMessage(lang.error("ERROR: Incorrect syntax, please use /rankup set [Player] [Rank]"));
							}
						}else if(args[0].equalsIgnoreCase("create")){
								if(args.length == 3){
									Player target = Bukkit.getPlayer(args[1]);
									if(target == null){
										sender.sendMessage(lang.error("ERROR: " + args[1] + " is null."));
									}else if(target.isOnline()){
										sender.sendMessage(prefix + ChatColor.AQUA + "Creating new player profile..");
										util.newConfigRankProfile(target, args[2], sender);
									}
								}else{
									sender.sendMessage(lang.error("ERROR: Incorrect syntax, please use /rankup create [Player] [New Rank]"));
								}
						}else if(args[0].equalsIgnoreCase("get")){
							if(args.length == 2){
								Player target = Bukkit.getPlayer(args[1]);
								if(target == null){
									sender.sendMessage(lang.error("ERROR: " + args[1] + " is null."));
								}else if(target.isOnline()){
									sender.sendMessage(prefix + ChatColor.AQUA + "Getting players profile...");
									util.getPlayerProfile(sender, target);
								}
							}
						}else{
							lang.description(sender);
						}
					}
				}
			}
			if(command.getName().equalsIgnoreCase("ranks")){
				if(sender instanceof Player){
					Player player = (Player) sender;
					if(player.hasPermission("prisonrankup.ranks")){
						sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix")) + "---------");
						sender.sendMessage("");
						sender.sendMessage(ChatColor.GOLD + "Current Available Ranks:");
						util.getAvailableRanks(player);
						sender.sendMessage("");
						sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix")) + "---------");
					}else{
						player.sendMessage(lang.noPermission);
					}
				}else{
					sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix")) + "---------");
					sender.sendMessage("");
					sender.sendMessage(ChatColor.AQUA + getConfig().getConfigurationSection("groups").getKeys(true).toString());
					sender.sendMessage("");
					sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', getConfig().getString("Prefix")) + "---------");
				}
			}
		return false;
	}
    
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		if(player.hasPermission("prisonrankup.update")){
			Updater updater = new Updater(this, 72819, this.getFile(), UpdateType.NO_DOWNLOAD, false);
			   if (updater.getResult() == UpdateResult.UPDATE_AVAILABLE) {
				    player.sendMessage(prefix + ChatColor.AQUA + "An update is available: " + Main.name + ", a " + Main.type + " for " + Main.version + " available at " + Main.link);
				    player.sendMessage(ChatColor.AQUA + "Type /rankup update if you would like to automatically update.");
			   }
		}
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
    private FileConfiguration customConfig = null;
    private File customConfigFile = null;
	
	public FileConfiguration getConfiguration() {
	    if (customConfig == null) {
	        reloadConfiguration();
	    }
	    return customConfig;
	}
	
	public void reloadConfiguration() {
	    if (customConfigFile == null) {
	        customConfigFile = new File(getDataFolder(), "config.yml");
	        }
	        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
	     
	        // Look for defaults in the jar
	        InputStream defConfigStream = this.getResource("config.yml");
	        if (defConfigStream != null) {
	            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	            customConfig.setDefaults(defConfig);
	        }
	}
	
	public void saveConfiguration() {
	        if (customConfig == null || customConfigFile == null) {
	            return;
	        }
	        try {
	            getConfiguration().save(customConfigFile);
	        } catch (IOException ex) {
	            getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
	        }
	}
}	
