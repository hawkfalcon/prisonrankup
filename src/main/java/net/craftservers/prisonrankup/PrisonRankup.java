package net.craftservers.prisonrankup;

import org.bukkit.entity.Player;

public class PrisonRankup {
	
	private static Util util = new Util();
	private static Main main = Main.getPlugin();
	
	public static String getNextRank(String playerName) {
		int count = 1;
		int max = 100;
		String rank = Main.getPlugin().getConfiguration().getString("First Rank");
		for(String key : Main.getPlugin().getConfiguration().getConfigurationSection("groups").getKeys(false)){
			if(count < max){
				if(key.equalsIgnoreCase(Main.getPlugin().getConfiguration().getString("users." + playerName + ".group"))){
					count = 110;
				}else{
					count++;
				}
			}else{
				count = 0;
				rank = key;
			}
		}
		return rank;
	}
/** Rank the player up (Will not check if they have balance)
    * @param player is the player that you want it to rankup
*/
	public static void rankPlayerUp(Player player) {
		String nextRank = getNextRank(player.getName());
		double rankPrice = Main.getPlugin().getConfiguration().getDouble("groups." + nextRank);
		util.rankplayerUp(nextRank, rankPrice, player);
	}
/** 
 * Rank the player up and will check if the player has balance. Will send the messages to the player if they do not
 * meet the requirements
 * @param player is the player you want to rankup 
 */
   public static void rankup(Player player) {
	   util.rankup(player);
   }
   
   public static String getRank(String playerName) {
	   return Main.getPlugin().getConfiguration().getString("users." + playerName + ".group");
   }
   /**
    * Get the rank price of a certain Rank
    * @param rank is the rank you want to get the price of
    * @return The price of the rank defined in the config
    */
   public static String getRankPrice(String rank) {
	   return Main.getPlugin().getConfiguration().getString("groups." + rank);
   }
   /** 
    * Creates a rank in the config.yml for PrisonRankup
    * @param rank is the rank you wish to create
    * @param rankPrice is the price you wish to set that rank
    */
   public static void createRank(String rank, double rankPrice){
	   Main.getPlugin().getConfiguration().getConfigurationSection("groups").createSection(rank);
	   Main.getPlugin().getConfiguration().set("groups." + rank, rankPrice);
	   main.reloadConfig();
   }
   /**
    * Create a profile under a players name
    * @param playerName is the name of the player profile you want to create
    * @param rank is the rank you wish to set the player profile to
    */
   public static void createProfile(String playerName, String rank) {
	   util.createAnonConfigRankProfile(playerName, rank);
   }
   /**
    * Check if the player is on the cooldown list
    * @param player is the player you are checking
    * @return if the player is on cooldown (Set in config)
    */
   public static boolean isOnCooldown(Player player) {
	   return TimeHandler.cooldown.contains(player);
   }
   /**
    * Check if they enabled the cooldown
    * @return if the cooldown is enabled or disabled
    */
   public static boolean cooldownEnabled() {
	   return Main.getPlugin().getConfiguration().getBoolean("Timed Requirement");
   }
   /**
    * Check if they enabled transfering ranks to profile
    * @return if the transfering process is enabled
    */
   public static boolean transferRanksEnabled() {
	   return Main.getPlugin().getConfiguration().getBoolean("Transfer ranks to profile");
   }
   
}
