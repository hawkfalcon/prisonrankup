package net.craftservers.prisonrankup;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class PRLang{
	
	/*                                                                                   
	 *                                                                                   *
	 *  This source is under the right of All Rights Reserved,                           *
	 *  You may not copy or edit this code or distribute it in any way without a license * 
	 *  from Mazen K.                                                                    *
	 * Copyright 2014 CraftServers. All Rights Reserved.                               *
	 *                                                                                   *
	 */                                                           
	
	Main main;
	public PRLang(Main instance){
		main = instance;
	}
	//private String prefix = ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfig().getString("Prefix"));
	public String noPermission = ChatColor.RED  + "You do not have permission to this command";
	public static String noPermissions = ChatColor.RED  + "You do not have permission to this command";
	public String error(String error){
		return main.prefix + ChatColor.RED + "" + ChatColor.BOLD + error; 
	}
	public void description(CommandSender sender){
		sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfig().getString("Prefix")) + "---------");
		sender.sendMessage("");
		sender.sendMessage(ChatColor.AQUA + "/rankup - " + ChatColor.GOLD + "Rankup to your wildest dreams!");
		sender.sendMessage(ChatColor.AQUA + "/rankup set [Player] [Rank] - " + ChatColor.GOLD + "Set a players rank (In the Rankup Configuration)");
		sender.sendMessage(ChatColor.AQUA + "/rankup create [Player] [Rank] - " + ChatColor.GOLD + "Create a new player profile");
		sender.sendMessage(ChatColor.AQUA + "/rankup get [Player] - " + ChatColor.GOLD + "Retrieve a players profile");
		sender.sendMessage("");
		sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfig().getString("Prefix")) + "---------");
	}
	public void descripter(CommandSender sender, String arg1, String arg2, String arg3){
		sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfig().getString("Prefix")) + "---------");
		sender.sendMessage("");
		sender.sendMessage(ChatColor.AQUA + arg1);
		sender.sendMessage(ChatColor.AQUA + arg2);
		sender.sendMessage(ChatColor.AQUA + arg3);
		sender.sendMessage("");
		sender.sendMessage(ChatColor.GREEN + "----------" + ChatColor.translateAlternateColorCodes('&', Main.getPlugin().getConfig().getString("Prefix")) + "---------");
	}
}
