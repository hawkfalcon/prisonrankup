package net.craftservers.prisonrankup;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinHandler implements Listener{
	
	Util util = new Util();
	
	Main plugin = Main.getPlugin();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		boolean rankbool = Main.getPlugin().getConfig().getBoolean("Transfer ranks to profile");
		if(rankbool) {
			if(!Main.getPlugin().getConfig().getConfigurationSection("users").contains(p.getName())){
                for(String key : Main.permission.getPlayerGroups(p)){
					if(Main.getPlugin().getConfig().getConfigurationSection("groups").contains(key)){
						util.createAnonConfigRankProfile(p.getName(), key);
					}
				}
			}
		}
	}

}
